import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { useHistory } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "../Button";
import { makeStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import LongMenu from "../LongMenu";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  toolbar: {
    minHeight: 128,
    alignItems: "flex-start",
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    alignSelf: "flex-end",
  },
}));

export default function Header() {
  const classes = useStyles();
  const preventDefault = (event) => event.preventDefault();
  const history = useHistory();

  function renderBack() {
    history.push("/blogs");
  }

  function renderNew() {
    window.location.href = "/blogs/new";
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h5" noWrap>
            Alkemy Challenge-React-Warm_UP
          </Typography>
          {localStorage.getItem("token") !== null && <LongMenu></LongMenu>}
        </Toolbar>
        {localStorage.getItem("token") !== null && (
          <Grid container spacing={3} justifyContent="center">
            <Grid item xs={3}>
              <Button
                label="Home"
                variant={"contained"}
                color={"danger"}
                size="small"
                handleClick={renderBack}
              />
            </Grid>
            <Grid item xs={3}>
              <Button
                label="Crear"
                variant={"contained"}
                color={"danger"}
                size="small"
                handleClick={renderNew}
              />
            </Grid>
          </Grid>
        )}
      </AppBar>
    </div>
  );
}
