import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Typography } from "@material-ui/core";
import axios from "axios";
import Environment from "../../enviroment";

const Detalle = ({ match }) => {
  const [currentBlog, setCurrentBlog] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    let data = JSON.parse(localStorage.getItem("blogs")).find(
      (blog) => blog.id === Number(match.params.id)
    );
    if (data === undefined) {
      setError("El ID recibido es invalido");
    } else {
      axios(`${Environment.api}posts/${match.params.id}`)
        .then((res) => {
          setCurrentBlog(data);
          setLoading(false);
        })
        .catch((error) => {
          setCurrentBlog(data);
          setLoading(false);
        });
    }
  }, []);

  return (
    <div>
      <br />
      <br />
      {error && (
        <div class="alert alert-danger" role="alert">
          <strong>{error}</strong>
        </div>
      )}
      {!error && loading === false && (
        <div>
          <br />
          <Typography variant="h4" className="center">
            {`Detalle del Post: N° ${currentBlog.id}`}
          </Typography>
          <br />
          <div className="center">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Caracteristica</th>
                  <th scope="col">Valor</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">
                    <h6>
                      <strong>N° Usuario</strong>
                    </h6>
                  </th>
                  <td>
                    <h6>{currentBlog.userId}</h6>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <h6>
                      <strong>Titulo del Post</strong>
                    </h6>
                  </th>
                  <td>
                    <h6>{currentBlog.title}</h6>
                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    <h6>
                      <strong>Contenido del Post</strong>
                    </h6>
                  </th>
                  <td>
                    <h6>{currentBlog.body}</h6>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      )}
      <br />
    </div>
  );
};

export default Detalle;
