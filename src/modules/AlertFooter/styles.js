import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  checkboxTypography: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "0px 0px 16px 24px",
  },
  footerFlex: {
    display: "flex",
  },
  footerButton: {
    margin: "0px 24px 16px 0px",
  },
  footerButtonSpace: {
    marginRight: theme.spacing(2),
  },
}));
export default useStyles;
