import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "../Button";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import { BlogService } from "../../services/BlogService";
import Environment from "../../enviroment";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import AlertDialog from "../AlertDialog";
//import { SnackbarContent, Snackbar } from "@material-ui/core";
import LoadingRequest from "../LoadingRequest";
import { CircularProgress } from "@material-ui/core";
import ConfirmationDialog from "../commons/ConfirmationDialog";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import "./index.css";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const Blog = () => {
  const [currentBlog, setCurrentBlog] = useState([]);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [closeAlert, setCloseAlert] = useState(false);
  const [recarga, setRecarga] = useState(false);
  const [loadingDelete, setLoadingDelete] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    setLoading(true);
    if (localStorage.getItem("blogs") === null) {
      BlogService.listar()
        .then((data) => {
          setLoading(false);
          localStorage.setItem("blogs", JSON.stringify(data));
          setRecarga(true);
          setRecarga(false);
        })
        .catch((error) => {
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  }, [openAlert, recarga]);

  const handleDelete = (blog) => {
    setOpenConfirm(true);
    setCurrentBlog(blog.id);
  };

  function onDelete() {
    setOpenConfirm(false);
    setLoadingDelete(true);
    axios
      .delete(`${Environment.api}posts/${currentBlog}`)
      .then(() => {
        let data = JSON.parse(localStorage.getItem("blogs")).filter(
          (blog) => blog.id !== currentBlog
        );
        localStorage.setItem("blogs", JSON.stringify(data));
        setOpenAlert(false);
        setOpenAlert(true);
        setCloseAlert(true);
        setLoadingDelete(false);
      })
      .catch((err) => {
        let data = JSON.parse(localStorage.getItem("blogs")).filter(
          (blog) => blog.id !== currentBlog
        );
        localStorage.setItem("blogs", JSON.stringify(data));
        setOpenAlert(false);
        setOpenAlert(true);
        setCloseAlert(true);
        setLoadingDelete(false);
      });
  }

  function editar(id) {
    window.location.href = `/blogs/edit/${id}`;
  }

  function detalle(id) {
    window.location.href = `/blogs/detail/${id}`;
  }

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        setCloseAlert(false);
      }}
      description="El Post se elimino exitosamente"
      actionAccept={() => setCloseAlert(false)}
      title=""
      alertType="success"
    />
  );

  return (
    <div>
      <br></br>
      <br></br>
      <ConfirmationDialog
        open={openConfirm}
        onConfirm={onDelete}
        onClose={() => setOpenConfirm(false)}
        message="¿Seguro que quieres eliminar este Post?"
      />
      <LoadingRequest open={loadingDelete} />
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>
                <strong>Titulo</strong>
              </TableCell>
              <TableCell>
                <strong>Acciones</strong>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading && (
              <div className="spinner">
                <CircularProgress />
              </div>
            )}
            {!loading &&
              localStorage.getItem("blogs") &&
              JSON.parse(localStorage.getItem("blogs")).map((blog) => (
                <TableRow key={blog.id}>
                  <TableCell component="th" scope="row">
                    {blog.title}
                  </TableCell>
                  <TableCell align="right">
                    <Grid container spacing={3}>
                      <Grid item xs={4}>
                        <Button
                          label="Detalle"
                          variant={"contained"}
                          color={"success"}
                          size="small"
                          handleClick={() => detalle(blog.id)}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Button
                          label="Editar"
                          variant={"contained"}
                          color={"primary"}
                          size="small"
                          handleClick={() => editar(blog.id)}
                        />
                      </Grid>
                      <Grid item xs={2}>
                        <IconButton
                          title="Eliminar"
                          onClick={() => handleDelete(blog)}
                        >
                          <DeleteIcon className={classes.icon} />
                        </IconButton>
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
        {openAlert && closeAlert && alert}
      </TableContainer>
    </div>
  );
};

export default Blog;
