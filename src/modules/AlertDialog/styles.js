import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: theme.spacing(62),
    minHeight: theme.spacing(24),
  },
  backdropBg: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  closeIcon: {
    fontSize: theme.spacing(3),
    position: "relative",
    float: "right",
    margin: "16px 24px 0px 0px",
  },
  body: {
    display: "flex",
    alignItems: "center",
    marginLeft: 32,
    marginTop: 32,
    color: theme.palette.text.primary,
  },
  footer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
  moveRight: {
    right: 0,
    width: "auto",
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "start",
    marginLeft: theme.spacing(3),
  },
}));
export default useStyles;
