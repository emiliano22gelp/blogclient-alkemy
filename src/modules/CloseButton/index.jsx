import React from "react";

import { useStyles } from "./styles";
import { CancelOutlined } from "@material-ui/icons";

const CloseButton = ({ handleClose, fontSize }) => {
  const classes = useStyles();

  return (
    <CancelOutlined
      className={classes.iconClose}
      onClick={handleClose}
      fontSize={fontSize}
    />
  );
};
export default CloseButton;
