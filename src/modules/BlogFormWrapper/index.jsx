import React, { useEffect, useState } from "react";
import Environment from "../../enviroment";
import BlogForm from "./BlogForm";
import AlertDialog from "../AlertDialog";
import LoadingRequest from "../LoadingRequest";
import axios from "axios";
import {
  Typography,
  CircularProgress,
  SnackbarContent,
  Snackbar,
} from "@material-ui/core";

const BlogFormWrapper = ({ match, history }) => {
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [loadingAction, setLoadingAction] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [openAlert2, setOpenAlert2] = useState(false);
  const [error, setError] = useState(null);
  const [currentBlog, setCurrentBlog] = useState({});

  useEffect(() => {
    if (match.params.id) {
      setLoading(true);
      let data = JSON.parse(localStorage.getItem("blogs")).find(
        (blog) => blog.id === Number(match.params.id)
      );
      if (data === undefined) {
        setError("El ID recibido es invalido");
      } else {
        axios(`${Environment.api}posts/${match.params.id}`)
          .then((res) => {
            setCurrentBlog(res.data);
            setLoading(false);
          })
          .catch((error) => {
            setCurrentBlog(data);
            setLoading(false);
          });
      }
    } else {
      setCurrentBlog({
        title: "",
        body: "",
      });
    }
  }, [match]);

  const handleSubmit = async (blog) => {
    setLoadingAction(true);
    setLoadingSubmit(true);
    let json = {};
    try {
      const res = match.params.id ? await update(blog) : await create(blog);
      setLoadingSubmit(false);
      setLoadingAction(false);
      if (!match.params.id) {
        json.userId = 1;
        const data = JSON.parse(localStorage.getItem("blogs"));
        json.id = Object.keys(data).length + 1;
        json.title = blog.title;
        json.body = blog.body;
        var nuevo = JSON.parse(localStorage.getItem("blogs"));
        nuevo.push(json);
        localStorage.setItem("blogs", JSON.stringify(nuevo));
        setOpenAlert(true);
      } else {
        let data = JSON.parse(localStorage.getItem("blogs")).find(
          (elem) => elem.id === Number(match.params.id)
        );
        data.title = blog.title;
        data.body = blog.body;
        var nuevo = JSON.parse(localStorage.getItem("blogs"));
        nuevo[match.params.id - 1] = data;
        localStorage.setItem("blogs", JSON.stringify(nuevo));
        setOpenAlert2(true);
      }
    } catch (error) {
      let data = JSON.parse(localStorage.getItem("blogs")).find(
        (elem) => elem.id === Number(match.params.id)
      );
      data.title = blog.title;
      data.body = blog.body;
      var nuevo = JSON.parse(localStorage.getItem("blogs"));
      nuevo[match.params.id - 1] = data;
      localStorage.setItem("blogs", JSON.stringify(nuevo));
      setOpenAlert2(true);
    }
  };

  const create = (blog) => axios.post(`${Environment.api}posts`, blog);
  const update = (blog) =>
    axios.put(`${Environment.api}posts/${match.params.id}`, blog);

  const handleCancel = () => {
    history.goBack();
  };

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        history.push("/blogs");
      }}
      description="El Post se dio de alta exitosamente"
      actionAccept={() => history.push("/blogs")}
      title=""
      alertType="success"
    />
  );

  const alert2 = (
    <AlertDialog
      open={openAlert2}
      handleClose={() => {
        history.push("/blogs");
      }}
      description="El Post ha sido actualizado"
      actionAccept={() => history.push("/blogs")}
      title=""
      alertType="success"
    />
  );

  return (
    <div>
      <br></br>
      <br></br>
      {error && (
        <div class="alert alert-danger" role="alert">
          <strong>{error}</strong>
        </div>
      )}
      {!error && (
        <div>
          <LoadingRequest open={loadingAction} />
          <Typography variant="h4" className="center">
            {match.params.id ? "Editar Post" : "Nuevo Post"}
          </Typography>
          {loading && (
            <div className="spinner">
              <CircularProgress />
            </div>
          )}
          {!loading && (
            <BlogForm
              onSubmit={handleSubmit}
              onCancel={handleCancel}
              loadingSubmit={loadingSubmit}
              currentBlog={currentBlog}
            />
          )}
        </div>
      )}
      {openAlert && alert}
      {openAlert2 && alert2}
    </div>
  );
};

export default BlogFormWrapper;
