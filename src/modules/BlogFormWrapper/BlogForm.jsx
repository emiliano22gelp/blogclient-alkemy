import React, { useEffect } from "react";
import { useFormik } from "formik";
import { Grid, TextField, Button } from "@material-ui/core";

const BlogForm = ({ onSubmit, onCancel, loadingSubmit, currentBlog }) => {
  /**
   * Se realizan todas las validaciones necesarias y se setea el objeto errors con los mensajes de error de cada campo
   */
  const validate = (values) => {
    const errors = {};
    if (!values.title) {
      errors.title = "Este campo es requerido";
    } else if (values.title.trim() === "") {
      errors.title = "Por favor ingrese algun cambio valido";
    } else if (/(\s{2,})/g.test(values.title)) {
      errors.title = "Por favor deje solo 1 espacio entre cada palabra";
    }
    if (!values.body) {
      errors.body = "Este campo es requerido";
    } else if (values.body.trim() === "") {
      errors.body = "Por favor ingrese algun cambio valido";
    } else if (/(\s{2,})/g.test(values.body)) {
      errors.body = "Por favor deje solo 1 espacio entre cada palabra";
    } else if (values.body.includes("\n")) {
      errors.body = "Por favor no presione saltos de linea";
    }

    return errors;
  };

  /**
   * Se crea una instancia de formik
   */
  const formik = useFormik({
    initialValues: currentBlog,
    onSubmit: (values) => onSubmit(values),
    validate,
  });

  useEffect(() => {
    formik.setValues(currentBlog);
  }, [currentBlog]);

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={5} justify="center">
        <Grid item xs={12} md={3}>
          <TextField
            id="code"
            label="Titulo"
            variant="outlined"
            className="input"
            name="title"
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.errors.title && formik.touched.title}
            helperText={formik.touched.title ? formik.errors.title : null}
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <TextField
            id="name"
            label="Contenido"
            variant="outlined"
            className="input"
            multiline
            rows={8}
            name="body"
            value={formik.values.body}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.errors.body && formik.touched.body}
            helperText={formik.touched.body ? formik.errors.body : null}
          />
        </Grid>
      </Grid>
      <br></br>
      <br></br>
      <Grid container spacing={5} justify="center">
        <Grid item>
          <Button
            variant="outlined"
            style={{ marginRight: 30 }}
            color="primary"
            onClick={onCancel}
            disabled={loadingSubmit}
          >
            Cancelar
          </Button>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disabled={loadingSubmit}
          >
            Aceptar
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default BlogForm;
