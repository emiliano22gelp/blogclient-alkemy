import React from "react";
import { Dialog, DialogContent, makeStyles } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: 50,
  },
  actionsContainer: {
    padding: theme.spacing(2, 3),
  },
}));

const LoadingRequest = ({ open }) => {
  const classes = useStyles();

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="xs"
      aria-labelledby="confirmation-dialog-title"
      open={open}
    >
      <DialogContent className={classes.container}>
        <CircularProgress />
      </DialogContent>
    </Dialog>
  );
};

export default LoadingRequest;
