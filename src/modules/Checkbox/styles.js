import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  noPadding: {
    padding: "0px",
  },
  label: {
    fontWeight: 500,
    fontSize: 12,
  },
}));
export default useStyles;
