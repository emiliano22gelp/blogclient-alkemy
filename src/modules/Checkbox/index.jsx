import React from "react";
import clsx from "clsx";

import CheckboxMUI from "@material-ui/core/Checkbox";
import InputLabel from "@material-ui/core/InputLabel";

import CustomCheckboxIcon from "./customCheckIcon";
import useStyles from "./styles";

const Checkbox = ({
  checked,
  defaultChecked,
  color = "primary",
  disabled,
  id,
  onChange,
  required,
  size = "medium",
  value,
  noPadding = false,
  label,
  name,
  inputRef,
}) => {
  const classes = useStyles();
  const checkbox = (id) => {
    return (
      <CheckboxMUI
        id={id}
        checked={checked}
        defaultChecked={defaultChecked}
        disabled={disabled}
        indeterminate={false}
        required={required}
        size={size}
        onChange={onChange}
        value={value}
        name={name}
        color={color}
        inputRef={inputRef}
        checkedIcon={<CustomCheckboxIcon />}
        classes={{
          root: clsx({
            [classes.noPadding]: noPadding,
          }),
        }}
      />
    );
  };
  return label ? (
    <div id={id}>
      {label && <InputLabel>{label}</InputLabel>}
      {checkbox()}
    </div>
  ) : (
    checkbox(id)
  );
};

export default Checkbox;
