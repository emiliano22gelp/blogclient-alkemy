import React from "react";
import { useStyles, customUseStyles } from "./styles";
import ButtonMUI from "@material-ui/core/Button";
import clsx from "clsx";

const Button = ({
  label,
  variant = "contained",
  color = "primary",
  size = "medium",
  startIcon,
  endIcon,
  handleClick,
  fullWidth = false,
  component = "button",
  disabled = false,
  to,
  type,
}) => {
  const classes = useStyles();
  const customClasses = customUseStyles();
  return (
    <ButtonMUI
      color={color !== "success" ? color : "inherit"}
      variant={variant}
      component={component}
      onClick={handleClick}
      startIcon={startIcon}
      size={size}
      endIcon={endIcon}
      to={to}
      type={type}
      fullWidth={fullWidth}
      disabled={disabled}
      disableRipple={true}
      className={clsx({
        [customClasses.containedSuccess]:
          color === "success" && variant === "contained",
        [customClasses.containedWarning]:
          color === "warning" && variant === "contained",
        [customClasses.textPrimary]: color === "primary" && variant === "text",
        inherit: !(
          (color === "success" || "warning") &&
          variant === "contained"
        ),
      })}
      classes={{ ...classes }}
    >
      {label}
    </ButtonMUI>
  );
};

export default Button;
