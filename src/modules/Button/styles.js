import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    textTransform: "capitalize",
    border: `1px solid transparent`,
    boxShadow: "none", // !important
    "&:hover, &:active": {
      boxShadow: "none",
    },
    minWidth: "268px",
    height: "36px !important",
    fontSize: "0.875em !important",
  },
  containedPrimary: {
    "&:hover": {
      backgroundColor: theme.palette.primary.dark,
    },
    "&:active": {
      backgroundColor: theme.palette.primary.light,
    },
  },
  containedSecondary: {
    backgroundColor: theme.palette.secondary.white,
    color: theme.palette.primary.main,
    "&:hover": {
      backgroundColor: theme.palette.secondary.white,
      color: theme.palette.primary.dark,
    },
    "&:active": {
      backgroundColor: theme.palette.action.press,
      color: theme.palette.secondary.white,
    },
  },
  outlinedPrimary: {
    backgroundColor: theme.palette.secondary.white,
    borderColor: theme.palette.secondary.white,
    color: theme.palette.text.secondary,
    "&:hover": {
      backgroundColor: theme.palette.action.press,
      borderColor: theme.palette.action.press,
      color: theme.palette.primary.main,
    },
    "&:active": {
      backgroundColor: theme.palette.action.press,
      borderColor: theme.palette.action.press,
      color: theme.palette.action.active,
    },
    "&.Mui-disabled": {
      border: "1px solid white",
    },
  },
  outlinedSecondary: {
    backgroundColor: "transparent",
    borderColor: "transparent",
    color: theme.palette.text.secondary,
    "&:hover": {
      backgroundColor: theme.palette.action.press,
      borderColor: theme.palette.action.press,
      color: theme.palette.primary.main,
    },
    "&:active": {
      backgroundColor: theme.palette.action.press,
      borderColor: theme.palette.action.press,
      color: theme.palette.action.active,
    },
    "&.Mui-disabled": {
      border: "1px solid transparent",
    },
  },
  sizeSmall: {
    minWidth: "8.5em",
  },
  sizeLarge: {
    minWidth: "21em",
  },
  // sizeExtraLarge: {
  //   minWidth: "31em",
  // },
}));

export const customUseStyles = makeStyles((theme) => ({
  containedSuccess: {
    backgroundColor: theme.palette.success.main,
    color: theme.palette.secondary.white,
    "&:hover": {
      backgroundColor: theme.palette.success.dark,
      color: theme.palette.secondary.white,
    },
    "&:active": {
      backgroundColor: theme.palette.success.light,
      color: theme.palette.secondary.white,
    },
  },
  containedWarning: {
    backgroundColor: theme.palette.warning.main,
    color: theme.palette.secondary.white,
    "&:hover": {
      backgroundColor: theme.palette.warning.dark,
      color: theme.palette.secondary.white,
    },
    "&:active": {
      backgroundColor: theme.palette.warning.light,
      color: theme.palette.secondary.white,
    },
  },
  textPrimary: {
    backgroundColor: "transparent",
    color: theme.palette.text.secondary,
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.action.active,
    },
    "&:active": {
      backgroundColor: "transparent",
      color: theme.palette.primary.main,
    },
  },
}));

export default useStyles;
