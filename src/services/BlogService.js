import Environment from "../enviroment";

export const BlogService = {
  listar: () => {
    return fetch(`${Environment.api}posts/`)
      .then((response) => response.json())
      .then((data) => {
        return data;
      })
      .catch((error) => {
        return error;
      });
  },
};
