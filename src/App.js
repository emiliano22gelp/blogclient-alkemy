import { Switch, Route, Redirect } from "react-router-dom";

//components
import Header from "./modules/header/Header";
import Home from "./modules/home/Home";
import Blog from "./modules/Blog";
import NotFound from "./modules/notFound/NotFound";
import BlogFormWrapper from "./modules/BlogFormWrapper";
import Detalle from "./modules/Detalle";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./styles/theme";
import "./App.css";

import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <div>
          <header>
            <Header />
          </header>
          <div className="container">
            <Switch>
              <Route exact path="/" component={Home}>
                {localStorage.getItem("token") && <Redirect to="/blogs" />}
              </Route>
              <Route exact path="/blogs" component={Blog}>
                {!localStorage.getItem("token") && <Redirect to="/" />}
              </Route>
              <Route path="/blogs/new" component={BlogFormWrapper}>
                {!localStorage.getItem("token") && <Redirect to="/" />}
              </Route>
              <Route path="/blogs/edit/:id" component={BlogFormWrapper}>
                {!localStorage.getItem("token") && <Redirect to="/" />}
              </Route>
              <Route path="/blogs/detail/:id" component={Detalle}>
                {!localStorage.getItem("token") && <Redirect to="/" />}
              </Route>
              <Route component={NotFound} />
            </Switch>
          </div>
        </div>
      </Router>
    </ThemeProvider>
  );
}

export default App;
