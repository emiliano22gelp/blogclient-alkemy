# IMPORTANTE: (LEER ANTES DE PROBAR LA APLICACION)

## Paso 1: Clonar el repositorio

## con http:

git clone https://emiliano22gelp@bitbucket.org/emiliano22gelp/blogclient-alkemy.git

### Paso 2: Instalar Dependencias

- cd "carpeta clonada"
- npm install

## Paso 3: Ejecutar la app en modo desarrollo

- npm run start
- Por defecto la app se levanta en el puerto 3000 [http://localhost:3000].

## Herramientas y Librerias utilizadas:

- Boostrap (para diseño responsive).
- Material UI (para la reutilizacion de componentes simples ya desarrollados, como por ejemplo, el componente TextField para simular un input de texto).
- Formik (para la validacion de formularios).
- Axios (para peticiones a la API).
- Hooks de estado y de efecto, useState y useEffect (para utilizar durante la renderizacion de componentes).
- localStorage (para almacenar datos que deben persistir mas alla del componente en el que son utilizados, por ej, el token del usuario autenticado).